import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductListingComponent } from './Components/product-listing/product-listing.component';
import { ProductDetailComponent } from './Components/product-detail/product-detail.component';

const routes: Routes = [
  { path: '', redirectTo: 'shop/new/all-new', pathMatch: 'full'  },
  { path: 'shop/new/all-new', component: ProductListingComponent },
  { path: 'shop/new/all-new/:productid', component: ProductDetailComponent },
  { path: '**', redirectTo: 'shop/new/all-new'  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

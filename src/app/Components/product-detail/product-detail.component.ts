import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductsService } from 'src/app/Services/products.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {
  pageid = '';
  data = {};

  constructor(private route: ActivatedRoute, private productService: ProductsService) { }

  ngOnInit() {
    this.pageid = this.route.snapshot.params.productid;
    this.productService.getAllProducts().subscribe((result: any) => {
      this.data = result.groups.find((item) => {
        return item.id === this.pageid;
      });
    });
  }
}

import { Component, OnInit } from '@angular/core';
import { ProductsService } from 'src/app/Services/products.service';

@Component({
  selector: 'app-product-listing',
  templateUrl: './product-listing.component.html',
  styleUrls: ['./product-listing.component.css']
})
export class ProductListingComponent implements OnInit {

  constructor(private productService: ProductsService) {}
  data = {};

  ngOnInit() {
    this.productService.getAllProducts().subscribe(
      (data) => {
        this.data = data;
      }
    );
  }

}

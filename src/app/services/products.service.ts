import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import constants from '../constants/routingConstants';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  
  constructor(private http: HttpClient) { }
  public getAllProducts() {
    return this.http.get(constants.endPoint);
  }
  
}
